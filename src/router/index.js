import Vue from 'vue';
import VueRouter from 'vue-router';
import AuthLayout from '@/layouts/Auth.vue';
import CommonLayout from '@/layouts/Common.vue';

import constants from '@/common/constants';

Vue.use(VueRouter);

const routes = [
  {
    path: '/auth',
    component: AuthLayout,
    children: [
      {
        path: 'login',
        component: () => import(/* webpackChunkName: "login" */ '@/views/auth/Login.vue'),
        name: constants.PAGES.LOGIN,
      },
    ],
  },
  {
    path: '/staff-management',
    component: CommonLayout,
    children: [
      {
        path: '/',
        name: constants.PAGES.STAFF_LIST,
        component: () => import(/* webpackChunkName: "staff-list" */ '@/views/staffs/List.vue'),
      },
    ],
  },
  {
    path: '/',
    component: CommonLayout,
    children: [
      {
        path: '/',
        name: constants.PAGES.HOME,
        component: () => import(/* webpackChunkName: "home" */ '@/views/Home.vue'),
      },
      {
        path: '*',
        component: () => import(/* webpackChunkName: "404" */ '@/views/common/404.vue'),
      },
    ],
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, _from, next) => {
  const loggedIn = localStorage.getItem('token');
  if (to.name !== 'LoginPage' && !loggedIn) {
    return next({ name: 'LoginPage' });
  }
  next();
  return true;
});

export default router;
