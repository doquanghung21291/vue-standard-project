import Vue from 'vue';
import Fragment from 'vue-fragment';
import VueAxios from '@/plugins/axios';
import App from '@/App.vue';
import router from '@/router';
import store from '@/store';
import vuetify from '@/plugins/vuetify';
import helpers from '@/common/helpers';
import constants from '@/common/constants';
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import i18n from './i18n';

const helpersPlugin = {
  install() {
    Vue.helpers = helpers;
    Vue.prototype.$helpers = helpers;
  },
};

const constantsPlugin = {
  install() {
    Vue.constants = constants;
    Vue.prototype.$constants = constants;
  },
};

Vue.use(Fragment.Plugin);
Vue.use(helpersPlugin);
Vue.use(constantsPlugin);
Vue.use(VueAxios);
Vue.use(Loading);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: (h) => h(App),
}).$mount('#app');
