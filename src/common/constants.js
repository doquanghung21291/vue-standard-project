const BASE_API_URL = process.env.VUE_APP_BASE_API_URL;
const APIS = {
  LOGIN: '/auth/login/',
  LOGOUT: '/auth/logout/',
  STAFF: {
    GET_LIST: '/staffs/',
  },
};
const PAGES = {
  STAFF_LIST: 'StaffListPage',
  LOGIN: 'LoginPage',
  HOME: 'HomePage',
};

export default {
  BASE_API_URL,
  APIS,
  PAGES,
};
