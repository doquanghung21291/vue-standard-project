import store from '@/store';
import router from '@/router';
import i18n from '@/i18n';

function showMessage(message, color) {
  store.commit('setSnack', { snack: message, color });
}

function showErrorMessage(message) {
  showMessage(message, 'error');
}

function showSuccessMessage(message) {
  showMessage(message, 'success');
}

function redirect(routerName) {
  router.push({ name: routerName });
}

function setLocale(locale) {
  i18n.locale = locale;
  localStorage.setItem('locale', locale);
}

function processErrorMessage(error) {
  let message = '';
  if (error.response.status === 404) {
    message = i18n.tc('apis.not_found');
  } else {
    message = error.response.data.message;
  }
  return message;
}

export default {
  showMessage,
  showErrorMessage,
  showSuccessMessage,
  redirect,
  setLocale,
  processErrorMessage,
};
