import { axios } from '@/plugins/axios';

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    async login(_context, payload) {
      try {
        const result = await axios.post(this._vm.$constants.APIS.LOGIN, {
          email: payload.email,
          password: payload.password,
        });

        localStorage.setItem('token', result.data.data);
        axios.defaults.headers.Authorization = `jwt ${result.data.data}`;
        return Promise.resolve();
      } catch (error) {
        return Promise.reject(this._vm.$helpers.processErrorMessage(error));
      }
    },

    async logout() {
      try {
        const result = await axios.post(this._vm.$constants.APIS.LOGOUT);
        if (JSON.parse(result.data.status) === true) {
          localStorage.removeItem('token');
        }
        return Promise.resolve();
      } catch (error) {
        return Promise.reject(this._vm.$helpers.processErrorMessage(error));
      }
    },
  },
};
