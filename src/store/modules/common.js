export default {
  namespaced: true,
  state: {
    drawer: true,
  },
  getters: {
  },
  mutations: {
    toggleDrawer(state) {
      state.drawer = !state.drawer;
    },
  },
  actions: {
  },
};
