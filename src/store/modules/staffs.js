import { axios } from '@/plugins/axios';

export default {
  namespaced: true,
  state: {
    staffs: [],
  },
  getters: {
    staffs(state) {
      return state.staffs;
    },
  },
  mutations: {
    setStaffs(state, payload) {
      state.staffs = payload.data;
    },
  },
  actions: {
    async getStaffs({ commit }) {
      try {
        const result = await axios.get(this._vm.$constants.APIS.STAFF.GET_LIST);
        commit('setStaffs', result.data);
        return Promise.resolve();
      } catch (error) {
        return Promise.reject(this._vm.$helpers.processErrorMessage(error));
      }
    },
    async createStaff(_context, payload) {
      try {
        await axios.post(this._vm.$constants.APIS.STAFF.GET_LIST, payload);

        // commit('setStaffs', result.data);
        return Promise.resolve();
      } catch (error) {
        return Promise.reject(this._vm.$helpers.processErrorMessage(error));
      }
    },
  },
};
