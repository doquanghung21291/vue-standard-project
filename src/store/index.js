import Vue from 'vue';
import Vuex from 'vuex';
import auth from '@/store/modules/auth';
import common from '@/store/modules/common';
import snackbar from '@/store/modules/snackbar';
import staffs from '@/store/modules/staffs';

Vue.use(Vuex);

Vue.config.devtools = true;
const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    common,
    auth,
    snackbar,
    staffs,
  },
  strict: debug,
});
