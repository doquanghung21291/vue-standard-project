import ax from 'axios';
import constants from '@/common/constants';

const token = localStorage.getItem('token');

const apiClient = ax.create({
  baseURL: constants.BASE_API_URL,
  withCredentials: false, // This is the default
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: `jwt ${token}`,
  },
});

export const axios = apiClient;

export default {
  // eslint-disable-next-line no-unused-vars
  install(Vue, options) {
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$axios = ax;
  },
};
